function C2 = layer2(f,c)
% layer2 produces output that corresponds to the 2nd layer
% - it is used as output-generating atom, this yields the scattering coefficients of
%   layer 1 (out1)
%INPUT
%   f = signal;
%   c = output of previous layer
%OUTPUT
%   C2 = output, taking the p-th row of each dgt (of each row) 
%   from layer1 
%%
%   if no output of layer1 is entered:
    if nargin<3
        c = layer1(f);
    end
       
%   dgt of eacht frequency channel of layer 1, then getting one new layer2
%   element C_p, by taking the first row of each dgt (of each row) from layer1
%   and creating a new spectrogram, i.e. performing low-pass filtering
    
    [k,l]=size(c);
    a = 16;
    M = 512;
    g=firwin('hann',M);
    for jj = 1:k
        c2 = (dgt(c(jj,:),g,a,M));

        C(jj,:) = c2(1,:);

    end
    C2 = C; 
%   visual output
    [m,n] = size(C);
    figure
    imagesc(real(C(1:m/2,:)))
    title('Output 1','FontSize', 18)
end
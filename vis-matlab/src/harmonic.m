
function esum = harmonic(a, omega, N)
%creates a signal with either soft, sharp or amplitude modulated envelope
%INPUT
%   a = either soft, sharp or modul
%   omega = fundamental frequency %ex.: 1500
%   N = number of harmonics %ex.: 10
%OUTPUT
%   esum = created signal;
%%
    
    L = 44100; %signal length
    k = 15000; %number of samples for overlap of basic shape 'h'
    h = window('hann',k)'; %basic envelope shape
    if strcmp(a,'soft') % soft envelope 
        s = [zeros(1,5000),h(1:k/2),ones(1,L-k-10000),h(k/2+1:end), zeros(1,5000)];
        An = s/max(s);
    elseif strcmp(a,'sharp') % sharp envelope
        s = [zeros(1,5000),ones(1,L-k/2-10000),h(k/2+1:end), zeros(1,5000)];
        An = s/max(s);
    elseif strcmp(a,'modul') % modulated envelope
        sc = [1:(L-10000)]/(L-10000)*20*pi;
        h=sin(sc);
        s = [zeros(1,5000),h, zeros(1,5000)];
        An = s/max(s);
    else 
        errordlg('Enter either "soft", "sharp" or "modul"!') % Warning
    end
    
    thetan = [1:L]/L;
    esum = 0; % building the tone with fundamental ferqu. and harmonics
    p = 1/(N);
    for n = 1:N
       esum = esum+ p*(N-n+1)*An.*(sin(2*pi*thetan*omega*n)); 
    end
end


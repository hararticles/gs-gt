function c = layer1(f)
% layer1 creats the output of a general Gabor transform. This corresponds to
% the first layer.
%INPUT
%   f = signal;
%OUTPUT
%   c = dgt(f); Gabor transform of the signal
%%
%   performing dgt on signal f
    a = 128;
    M = 2048;
    g=firwin('hann',M);
    c = dgt(f,g,a,M);
    [m,n] = size(c);
%   plotting only the negative harmonics
    figure
    imagesc(abs(c(1:m/2,:)))
    title('Layer 1','FontSize', 18)
end
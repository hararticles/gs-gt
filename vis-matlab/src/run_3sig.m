function [c,C2,C3,cs,C2s,C3s,cm,C2m,C3m]=run_3sig(p)
% run_3sig runs layer1,layer2,layer3 in order to get the Gabor transform and
% the coefficients of the Gabor scattering transform with different
% envelopes (of 'harmonic').
%INPUT
%   p = row of layer1 that is developed until layer2 
%OUTPUT
%   c,C2,C3 = outputs from the different layers, with sharp envelope
%   cs,C2s,C3s = outputs from the different layers, with soft envelope
%   cm,C2m,C3m = outputs from the different layers, with modulated envelope
%%

if nargin == 0
    p = 70;
end

% fundamental frequency and number of harmonics
omega = 1500;
N = 13;

% signal with sharp attack
esum = harmonic('sharp',omega,N);
c = layer1(esum);
C2 = layer2(esum,c);
C3 = layer3(p,c);

% signal with smooth attack
esums = harmonic('soft',omega,N);
cs = layer1(esums);
C2s = layer2(esums,cs);
C3s = layer3(p,cs);

% signal with amplitude modulation
esumm = harmonic('modul',omega,N);
cm = layer1(esumm);
C2m = layer2(esumm,cm);
C3m = layer3(p,cm);
end
function C3 = layer3(p,c)
% layer3 computes elements belonging to layer3
% - is used as output-generating atom, this yields the scattering coefficients of
%   layer 2 (out2)
%INPUT
%   p = the frequency channel of layer1 to build a layer2 element.
%   c = output of the first layer (layer1)
%OUTPUT
%   C3 = output, taking the dgt of the p-th row of the first layer (= layer2 - element) and 
%   performing a dgt (of each row) from the  2nd layer and take only the first row  of each output, i.e. low-pass filtering.
%%

%   dgt of the p-th row of layer 1  = 2nd layer-element
    a = 1;
    M = 440;
    g=firwin('hann',100);
    c2 = abs(dgt(c(p,:),g,a,M));


    [k,l]=size(c2);
    
%   calculating the dgt of each row of c2 and then taking only the first
%   row of each dgt of c2 - this corresponds to low-pass filtering
    for jj = 1:k
        a = 1;
        M = 256;
        g=firwin('hann',M);
        c3 = (dgt(c2(jj,:),g,a,M));
        C(jj,:) = c3(1,:);

    end
    C3 = C; 
    
%   visual output
    [m,n] = size(C);
    figure
    imagesc(real(C(1:m/2,:)))
    title('Output 2','FontSize', 18)

end
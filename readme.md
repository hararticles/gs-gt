# Gabor frames and deep scattering networks in audio processing

## Demonstration of the experiments for reproducibility

Article: "Gabor frames and deep scattering networks in audio processing" <br> 
Authors: R. Bammer, M. Dörfler, P. Harar <br> 
Submitted to: Submitted to a special issue (Harmonic Analysis and Applications) of Axioms (ISSN 2075-1680) <br>
arXiv: https://arxiv.org/abs/1706.08818

## What is in this repository

In this repository you can find experiments with Gabor scattering time-frequency representation and its visualizations. The main purpose of this repository is to serve as a supplementary material for the scientific article mentioned above. Note, that this is not a repository containing the Gabor scattering library itself, you can find the implementation here: https://gitlab.com/paloha/gabor-scattering. 

This repository consists of the following main parts:

1. Experiments on synthetic data in Python (```synthetic``` folder)
2. Experiments on Good Sounds data in Python (```goodsounds``` folder)
3. Early implementation of Gabor scattering in Matlab with some visualizations (```vis-matlab``` folder)
4. Hypergrid interactive visualization of Gabor scattering outputs (```vis-hypergrid``` folder)

In order to reproduce the results presented in the aforementioned paper, you can preprocess the data and train the networks as in 1. and 2. by opening the jupyter notebooks and following the instructions.

## Installation
If you want to train the networks, ideally you should have a GPU and SSD available.
Code was developed and tested only on OS Ubuntu 18.04.

Installation within virtualenv:

* ```git clone git@gitlab.com:hararticles/gs-gt.git```
* ```cd gs-gt```
* ```virtualenv .venv```
* ```source .venv/bin/activate```
* ```pip install -r requirements.txt``` 
* ```jupyter notebook```
* open ```synthetic``` or ```goodsounds``` and follow the ```.ipynb``` notebook 

Installation with conda:

* ```conda create --name venv python=3.6 tensorflow-gpu=1.12.0 keras=2.2.4 pip```
* ```source activate venv```
* open the ```requirements.txt``` and remove lines containing tensorflow-gpu and keras
* ```pip install -r requirements.txt```
* ```jupyter notebook```
* open ```synthetic``` or ```goodsounds``` and follow the ```.ipynb``` notebook 

## License
This project is licensed under the terms of the MIT license. See license.txt for details.

## Acknowledgement
This work was supported by International Mobility of Researchers (CZ.02.2.69/0.0/0.0/16027/0008371) and by project LO1401. The infrastructure of the SIX Center was used.
![opvvv.2.2](/uploads/f532b2ae6e6c137556906cd3ed274b52/opvvv.2.2.jpg)

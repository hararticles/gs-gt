

######### REPRODUCIBILITY #########
# FOR REPRODUCIBLE RESULTS RUN THIS USING: PYTHONHASHSEED=0 python run-experiment.py
import numpy as np
import tensorflow as tf
import random as rn
np.random.seed(42)
rn.seed(12345)
tf.set_random_seed(1234)
# Really reproducible results are obtainable only
# by running the tf code in single thread and on CPU.
# For GPU it is not possible, but we do what we can.
###################################

config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)

import os, time, ast
from src.utils import Memmap_Sequence, EvaluateOnSequence
from keras.models import Model
from keras.layers import Input, Conv2D, Dense, Activation, Dropout, Flatten
from keras.layers import AveragePooling2D, Reshape, Permute, Lambda
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from keras.callbacks import ModelCheckpoint, EarlyStopping, CSVLogger
import keras.backend as K
from keras import regularizers

######## EXPERIMENT CONFIG ########
experiment_id = os.environ['EXPID']
config_index = os.environ['CID']
data_path = os.environ['DATAPATH']
rep = os.environ['REP']
rep_shape = ast.literal_eval(os.environ['REPSHAPE'])
trainsamples = int(os.environ['NTRAIN'])
validsamples = int(os.environ['NVALID'])
trainseed = int(os.environ['TSEED'])
validseed = int(os.environ['VSEED'])
epochs = int(os.environ['EPOCHS'])
batch_size = int(os.environ['BATCHSIZE'])
tms0 = os.environ['TMS0']
vms0 = os.environ['VMS0']
scalef = os.environ['SCALEF']
reg = float(os.environ['REG'])


###################################
# Create a directory where the results will be stored
experiment_path = 'experiments/{}_{}_C{}-t{}-v{}-e{}'.format(experiment_id,
    rep, config_index, trainsamples, validsamples, epochs)
if not os.path.exists(experiment_path):
    os.makedirs(experiment_path)

# Train sequence
train_memmap_name = 'memmap_{}_{}x{}.dat'.format(rep, tms0, np.prod(rep_shape))
train_memmap_path = os.path.join(data_path, 'train', '{}-C{}'.format(trainseed, config_index), train_memmap_name)
train_seq = Memmap_Sequence(train_memmap_path, trainsamples, batch_size, rep_shape)

# Validation sequence
valid_memmap_name = 'memmap_{}_{}x{}.dat'.format(rep, vms0, np.prod(rep_shape))
valid_memmap_path = os.path.join(data_path, 'valid', '{}-C{}'.format(validseed, config_index), valid_memmap_name)
valid_seq = Memmap_Sequence(valid_memmap_path, validsamples, batch_size, rep_shape)

# Scaling functions for data that run on GPU
def logGpu(x):
    return K.log(x + 1e-08)

def identity(x):
    return x

scaling_functions = {'log': logGpu, 'identity': identity}

# Definition of the CNN architecture
def get_model(nkernel=16):
    inputs = Input(batch_shape=(None, *rep_shape))
    a = Lambda(scaling_functions[scalef])(inputs) 
    if len(rep_shape) == 3:
        is_scattering = True
        a = Permute((2,3,1))(a)
    elif len(rep_shape) == 2:
        is_scattering = False
        a = Reshape((*rep_shape, 1))(a)
    else:
        raise ValueError('REPSHAPE must be of len 2 or 3, instead of {} for {}.'.format(len(rep_shape), rep_shape))
    
    var_pool = 1 if rep in ['MS', 'MT'] else 2
    reg_dens = regularizers.l2(reg) if reg else None

    a = BatchNormalization(axis=-1)(a)
    a = Conv2D(nkernel, kernel_size=(3,3), strides=(1,1),padding='same', data_format='channels_last', activation='relu')(a)
    a = AveragePooling2D(pool_size=(var_pool, 2), strides=None, padding='same', data_format='channels_last')(a)

    a = Conv2D(nkernel, kernel_size=(3,3), strides=(1,1),padding='same', data_format='channels_last', activation='relu')(a)
    a = AveragePooling2D(pool_size=(2, 2), strides=None, padding='same', data_format='channels_last')(a)

    a = Conv2D(nkernel, kernel_size=(3,3), strides=(1,1),padding='same', data_format='channels_last', activation='relu')(a)
    a = AveragePooling2D(pool_size=(2, 2), strides=None, padding='same', data_format='channels_last')(a)

    a = Conv2D(8, kernel_size=(3,3), strides=(1,1),padding='same', data_format='channels_last', activation='relu')(a)
    a = AveragePooling2D(pool_size=(4, 4), strides=None, padding='same', data_format='channels_last')(a)

    a = Flatten()(a)
    outputs = Dense(4, kernel_regularizer=reg_dens, activation='softmax')(a)
    return Model(inputs, outputs)

model = get_model()

def myprint(s):
    with open(os.path.join(experiment_path, 'modelsummary.txt'),'a') as f:
        print(s, file=f)

model.summary(print_fn=myprint) # Save model summary to text file
#print(model.summary()) # Print model summary to console

# Set desired optimizer and loss function and compile
optimizer = Adam()
model.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['acc'])

callbacks = [
             EvaluateOnSequence(train_seq, 'train'), # We want the real accuracy on train, not averaged
             CSVLogger(os.path.join(experiment_path, 'training.csv')),
             ModelCheckpoint(os.path.join(experiment_path, '{epoch:02d}-{val_acc:.4f}-{train_acc:.4f}.model'), monitor='val_acc', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)]

s1 = time.time()
np.random.seed(42) # Set the random seed for shuffling the batches
history = model.fit_generator(train_seq,
                              epochs=epochs,
                              verbose=1,
                              callbacks=callbacks,
                              validation_data=valid_seq,
                              class_weight=None,
                              max_queue_size=10,
                              workers=8,
                              use_multiprocessing=True,
                              shuffle=True,
                              initial_epoch=0)
tt = time.time() - s1
myprint('Total training time: {}s. Per epoch: {}s.'.format(tt, tt/epochs))

from gabor_scattering import gs
from librosa.feature import melspectrogram
import numpy as np


def oscilator(dur, rate, a=1, f=1, p=0, a_fm=0, f_fm=1, p_fm=0, a_am=0, f_am=1, p_am=0):
    """
    Generates pure sine wave with possibility of amplitude and/or frequency modulation.
    The implementation of frequency modulation in theory uses cos function, but
    to avoid integration we used its integrated function = sin.

    Parameters
    --------

    dur: float
        The duration of generated signal in seconds
    rate: int
        Sampling rate of generated signal in Hz
    a: float
        Amplitude of signal
    f: float
        Frequency of signal
    p: float
        Phase of generated signal in radians
    a_fm: float
        Amplitude of frequency modulation carrier wave
    f_fm: float
        Frequency of frequency modulation carrier wave
    p_fm: float
        Phase of frequency modulation carrier wave in radians
    a_am: float
        Amplitude of amplitude modulation carrier wave
    f_am: float
        Frequency of amplitude modulation carrier wave
    p_am: float
        Phase of amplitude modulation carrier wave in radians
    """

    t = np.arange(0, dur, 1/rate)
    cw_fm = a_fm * np.sin(2.0 * np.pi * f_fm * t + p_fm)
    cw_am = a_am * np.sin(2.0 * np.pi * f_am * t + p_am) if a_am > 0 and (p_am > 0 or f_am > 0) else 1
    g = a * np.sin(2.0 * np.pi * (f * t + cw_fm + p_fm) + p) * cw_am
    return t, g


def generate_random_baseparams(seed):
    """
    Infinit generator of parameters that describe the fundamental sine wave.
    Parameters include only frequencies and phases. Amplitudes are None.
    Random state can be set using seed. Yields a dictionary of parameters.
    """
    np.random.seed(seed)
    while True:
        yield {
            'a': None,
            'f': np.random.randint(100, 4000),
            'p': np.random.uniform(0, 2*np.pi),
            'a_fm': None,
            'f_fm': np.random.randint(10, 50),
            'p_fm': np.random.uniform(0, 2*np.pi),
            'a_am': None,
            'f_am': np.random.randint(1, 20)/4,
            'p_am': np.random.uniform(0, 2*np.pi),
        }

        
def get_nth_harmonic_params(params, n, a_divisor=1.3):
    """
    Changes amplitude and frequency in baseparams in order to get params of the desired harmonic.
    If n = 0, returns params with amplitude 1 and unchanged frequency.
    """
    params = params.copy()
    params['a'] = 1 / a_divisor ** n
    params['f'] *= n+1
    return params


def get_paramslist(params, nharmonics):
    """
    Returns list of dictionaries containing parameters of all signal components of the final signal.
    """
    paramslist = []
    for n in range(nharmonics):
        paramslist.append(get_nth_harmonic_params(params, n))
    return paramslist


def harmonizer(dur, rate, paramslist):
    """
    Generates signal of desired duration with specified sampling rate based on list of parameters.
    """
    signal = np.zeros(int(dur*rate))
    for p in paramslist:
        signal += oscilator(dur, rate, **p)[1]
    return signal


def assign_class(paramslist, nclass):
    """
    Adjusts amplitudes of amplitude modulation and frequency modulation in
    all dictionaries of paramlist according to 4 preselected classes.
    """
    a_fm, a_am = [(0, 0), # Class 0 fm_OFF am_OFF
                  (0, 1), # Class 1 fm_OFF am_ON
                  (1, 0), # Class 2 fm_ON  am_OFF
                  (1, 1)  # Class 2 fm_ON  am_ON
                 ][nclass]

    for p in paramslist:
        p['a_fm'] = a_fm
        p['a_am'] = a_am

    return paramslist


def generate_four_classes(dur, rate, nharmonics, seed, tfparams, yield_wave, 
                          yield_params, memmap_shapes, start_i=0, stop_i=None): 
    """
    Generator that generates specified number of random samples (signals of 4 classes) based on seed.
    Can return raw waveform, waveform parameters, multiple Gabor scattering time-frequency representations 
    based on parameters their names and parameters in tfparams dictionary.
    """
    while True:
        for i, p in enumerate(generate_random_baseparams(seed)):

            if stop_i is not None and i == stop_i: break # End of an epoch

            if i < start_i:
                # If we want to skip some of the first samples in gen, we need
                # to generate the random params anyways to change the state of
                # the random generator, but we skip a lot of computation ahead
                continue

            paramslist = get_paramslist(p, nharmonics=nharmonics)
            for nclass in range(4):
                
                # Get parameters of desired class
                cp = assign_class(paramslist, nclass)
                
                # Generate the signal based on parameters
                wave = harmonizer(dur, rate, cp)
                
                # Initialize output dict
                output = {'i': i, 'nclass': nclass,
                          'wave': wave if yield_wave else None,
                          'params': cp if yield_params else None}
                
                # Update output dict with desired outputs
                
                for tf in tfparams.keys():
                    x = gs(wave, **tfparams[tf])
                    _ = [out for out in [x.get('layer1'), x.get('layer2'), x.get('l2avg'), 
                                         x.get('out1'), x.get('out2'), x.get('outA'),
                                         x.get('outB'), x.get('outC')] if out is not None]
                    try:
                        # If all outs in data are of the same shape, make it a np.array
                        output[tf] = np.array(_)
                    except:
                        # If all outs are not the same shape, return just a list
                        output[tf] = _

                yield output